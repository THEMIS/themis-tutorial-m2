# Common Plotting
source("../common.r")

# Set up graphs
cols<- "OrRd" #Default colormap
leg <- "none" #Default Legend
lsize <- 10   #Legend size

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("data.dat", header = TRUE)


# Basic Corrections for Metrics

# Adjust Min delay to 0 if there was no resolutions (metric will display max trace length)
dat$min_delay = ifelse(dat$resolutions == 0, 0, dat$min_delay)

# Adjust Length of Run, algorithms abort on a round + 1
dat$run_len   = ifelse(is.na(dat$verdict), dat$run_len, dat$run_len - 1)

# Generate Data Frame for metrics graphs

res<- dat %>%
    group_by(alg, comps) %>%
  mutate(
    delay=zerodiv(sum_delay, resolutions)
  ) %>%
  summarise(
    delay_avg_1=mean(delay), delay_avg_2=sd(delay),
    msg_num_1=mean(zerodiv(msg_num,run_len)),  msg_num_2=sd(zerodiv(msg_num,run_len)),
    msg_data_1=mean(zerodiv(msg_data,run_len)),  msg_data_2=sd(zerodiv(msg_data,run_len)),
    simpm_1=mean(simp_maxmon), simpm_2=sd(simp_maxmon),
    simpc_1=mean(zerodiv(simp_maxmon_crit,run_len)), simpc_2=sd(zerodiv(simp_maxmon_crit,run_len)),
    conve_1=mean(zerodiv(exp_conv_comp,run_len)),  conve_2=sd(zerodiv(exp_conv_comp,run_len)),
    len=mean(run_len),
    verdictf=getverdict(verdict)
  ) %>%
  mutate(
    delay_avg=displaystat(delay_avg_1, delay_avg_2),
    msg_num=displaystat(msg_num_1, msg_num_2),
    msg_data=displaystat(msg_data_1, msg_data_2),
    #      simpt=displaystat(simpt_1, simpt_2),
    simpm=displaystat(simpm_1, simpm_2),
    simpc=displaystat(simpc_1, simpc_2),
    #    expt=displaystat(expt_1, expt_2),
    conve=displaystat(conve_1, conve_2)
  ) %>%
  select(alg, comps,
         #verdictf,
         #delay_max,
         delay_avg,
         #mdelay,
         msg_num, msg_data,
         #simpt,
         simpm,
         simpc,
         #expt,
         #expm,
         #convs,
         conve
         #len
  )

message("Writing table to table.tex")
print(xtable(res), file="table.tex", include.rownames=FALSE)


# Generate Metric Plots
gg <- themis_plot(dat, "zerodiv(msg_num, run_len)", "#Msgs  (Normalized)",  type=themis_box_nonotch, pal=cols, legend=leg) + coord_cartesian(ylim=c(0,8.5))
ggsave("msgnum.pdf", plot=gg)

gg <- themis_plot(dat, "zerodiv(msg_data, msg_num)", "Data per Message", pal=cols, legend=leg) + coord_cartesian(ylim=c(0,420))
ggsave("msgdatanorm.pdf", plot=gg)

gg <- themis_plot(dat, "simp_maxmon", "Maximum Simplifications per Monitor (Worst-Case)", pal=cols, legend=leg)  + coord_cartesian(ylim=c(0,50))
ggsave("compmon.pdf", plot=gg)


gg <- themis_plot(dat, "zerodiv(simp_maxmon_crit, run_len)", "Critical Simplifications (Normalized)", pal=cols, legend=leg)  +
  coord_cartesian(ylim=c(0,30))
ggsave("compcrit.pdf", plot=gg)


gg <- themis_plot(dat, "zerodiv(sum_delay, resolutions)", "Average Delay",
            pal=cols, type=themis_box_nonotch, legend="top")   +
  coord_cartesian(ylim=c(0,5)) +
  theme(legend.text=element_text(size=lsize)) +
  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal")
ggsave("delay.pdf", plot=gg)

gg <- themis_plot(dat, "zerodiv(msg_data, run_len)", "Data  (Normalized)",
            pal=cols) +
  coord_cartesian(ylim=c(0,420)) +
  theme(legend.text=element_text(size=lsize)) +
  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal")
ggsave("msgdata.pdf", plot=gg)

#themis_plot(dat, "max_delay", "Max Delay", pal=cols,type=themis_box_nonotch)  + coord_cartesian(ylim=c(0,7.5))
gg <- themis_plot(dat, "zerodiv(exp_conv_comp, run_len)", "Convergence", pal=cols, legend=leg, type=themis_box_nonotch)
ggsave("convergence.pdf", plot=gg)

# Compare data across traces


# LaTeX Tables
# Generate data frame
restrace <- dat %>%
  filter(comps==6) %>%
  group_by(alg, tracedir) %>%
  mutate(
    delay=zerodiv(sum_delay, resolutions)
  ) %>%
  summarise(
    delay_max=max(delay),
    delay_avg_1=mean(delay), delay_avg_2=sd(delay),
    mdelay_1=mean(max_delay), mdelay_2=sd(max_delay),
          msg_num_1=mean(msg_num/run_len),  msg_num_2=sd(msg_num/run_len),
          msg_data_1=mean(msg_data/run_len),  msg_data_2=sd(msg_data/run_len),
          simpt_1=mean(simp_total/run_len), simpt_2=sd(simp_total/run_len),
          simpm_1=mean(simp_maxmon), simpm_2=sd(simp_maxmon),
          simpc_1=mean(zerodiv(simp_maxmon_crit, run_len)), simpc_2=sd(zerodiv(simp_maxmon_crit, run_len)),
          expt_1=mean(exp_total/run_len), expt_2=sd(exp_total/run_len),
          expm_1=mean(exp_maxmon), expm_2=sd(exp_maxmon),
    convs_1=mean(simp_conv_comp/run_len),  convs_2=sd(simp_conv_comp/run_len),
    conve_1=mean(exp_conv_comp/run_len),  conve_2=sd(exp_conv_comp/run_len),
    len=mean(run_len)
    #     verdictf=getverdict(verdict)
  ) %>%
  mutate(
    delay_avg=displaystat(delay_avg_1, delay_avg_2),
    delay_max=sprintf("%.2f", delay_max),
    mdelay=displaystat(mdelay_1, mdelay_2),
        msg_num=displaystat(msg_num_1, msg_num_2),
        msg_data=displaystat(msg_data_1, msg_data_2),
        simpt=displaystat(simpt_1, simpt_2),
        simpm=displaystat(simpm_1, simpm_2),
        simpc=displaystat(simpc_1, simpc_2),
        expt=displaystat(expt_1, expt_2),
        expm=displaystat(expm_1, expm_2),
    convs=displaystat(convs_1, convs_2),
    conve=displaystat(conve_1, conve_2)
  ) %>%
  select(alg, tracedir,
         #verdictf,
         #delay_max, delay_avg,
         mdelay,
         msg_num, msg_data,
         #simpt,
         #simpm,
         simpc,
         #expt, expm,
         #convs,
         #len,
         conve
    )
# Generate table
message("Writing table to tabletrace.tex")
print(xtable(restrace), file="tabletrace.tex", include.rownames=FALSE)
