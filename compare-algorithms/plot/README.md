# Plotting Results

This folder contains the necessary data and scripts needed to generate the results we used in the paper (and more).
It also creates the bootstrap for people to create their own benchmark.
For the scope of this tutorial, simply refer to the [bench](bench/) folder.

### Dependencies
> If you are running inside the docker, the dependencies are already installed.

We use [`R`](https://www.r-project.org/) to compile our data with the following library dependencies:

* ggplot2
* reshape2
* dplyr
* xtable  


### Compiling Results


* Make sure you are in the [bench](bench/) folder:

```bash
cd plot/bench
```
* [data.dat](bench/data.dat) contains the summary of the data needed for our paper
* To generate the plots from [data.dat](bench/data.dat) run: (this uses the following [R Script](bench/plot.r))

```bash
make plot
```
* You will notice in the output  LaTeX tables, and graphs will be created in the `plot/bench` folder as `pdf` files.

> **Note (Docker users)**: You can browse the files outside docker using any filebrowser and programs at your disposal to inspect them.

### Deleting All Figures
To delete the charts run:

```bash
make clean
```

### Reconstructing the Data file
```
* To generate [data.dat](bench/data.dat) from `run.db` run: (~ 1 minute)

```bash
cd plot/bench
make gendata
```

> [data.dat](bench/data.dat) is the result from running [query.sql](bench/query.sql) on the raw experimental data.
