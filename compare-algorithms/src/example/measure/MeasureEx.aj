package example.measure;

import uga.corse.themis.measurements.Instrumentation;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.measurements.Measure;
import uga.corse.themis.measurements.Measures;
import uga.corse.themis.runtime.Node;

import java.io.Serializable;
import java.util.Map;

public aspect MeasureEx extends Instrumentation {

  @Override
	protected void setup(Node n, Map<String, Serializable> tags) {
    //Create a category for our measures
		setDescription("Example Measure");

    //Create a memory measure with
    //key:  "ex_memory" the key is used for its db column name, and for logging
    //name: "Memory Access" this is for the user to read
    //initial value: 0
    //update function: integer addition, this is called whenever update is called
		addMeasure(new Measure("ex_memory", "Memory Access",  0, Measures.addInt));
	};

  // Aspect J: bound after a call to a Memory.get 
  // and update memory access by adding 1
	after() : call(* Memory.get(..)) {
		update("ex_memory", 1);
	}
}
