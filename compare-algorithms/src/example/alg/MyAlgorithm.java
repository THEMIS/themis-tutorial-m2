package example.alg;

import java.util.TreeSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uga.corse.themis.Topology;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.ehe.Representation;
import uga.corse.themis.monitoring.specifications.SpecAutomata;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.utils.Convert;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;

/**
 * Migration Monitoring Algorithm
 * Skeleton
 */
public class MyAlgorithm implements StandardMonitoring {
    protected Automata aut;

    @Override
    public void setSpecification(Map<String, Specification> specs) {

        //Our main specification will be named "root"
        Specification main = specs.get("root");
        if(main == null)
            throw new IllegalArgumentException("No Root Spec found");

        //Convert it to Automata
        setMainSpec(Convert.makeAutomataSpec(main));
    }

    public void setMainSpec(Specification spec) {
        if(!(spec instanceof SpecAutomata))
            throw new IllegalArgumentException("Specification must be an automaton!");
        aut = ((SpecAutomata) spec).getAut();
        if(aut == null)
            throw new IllegalArgumentException("Automaton is not valid");

        //Simplify the automaton transitions
        aut = Convert.simplifyTransitions(aut);
    }

    @Override
    public Set<? extends Monitor> getMonitors(Topology topology) {


        int startID = getStartID(aut);
        int c = topology.getCountComponents();

        Set<MyMonitor> mons = new HashSet<>(c);

        // Generate one monitor per component
        Integer i = 0;
        // Create one monitor per component (node)

        // Note that this isn't sorted so might need to sort it as well
        TreeSet<String> sorted = new TreeSet<>(topology.getGraph().keySet());

        // Go over components
        for (String comp : sorted) {

            //Create a new monitor
            MyMonitor mon = newMonitor(i, aut, c);
            //Associate the monitor with the component
            mon.setComponentKey(comp);
            //Add it to our set of monitors
            mons.add(mon);
            //Init: If monitor is expected to start monitor toggle it on
            if(startID == i) mon.setStart(true);
            //Increment ID by 1
            i++;
        }

        return mons;
    }

    protected MyMonitor newMonitor(int id, Automata aut, Integer comps) {
      return new MyMonitor(id, aut, comps);
    }

    // Expand EHE and see
    // if we are the monitor supposed to start monitoring first
    public static int getStartID(Automata aut) {

            Representation ehe = new Representation(aut);

            // Move one to determine if we should start monitoring
            ehe.tick();

            // Find next obligation
            AtomObligation early = Expressions.findEarliestObligation(ehe, true);

            // The default symbol mapper maps a to ID 0, b to 1 etc.
            // Trace structure for atoms is a0,a1 for two observations of component a
            return (new DefaultSymbolMapper()).monitorFor(early);
    }
}
