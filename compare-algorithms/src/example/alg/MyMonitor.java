package example.alg;


import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.VerdictTimed;
import uga.corse.themis.comm.packets.RepresentationPacket;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.monitoring.MonitorCallback;
import uga.corse.themis.monitoring.MonitorRound;
import uga.corse.themis.monitoring.memory.*;
import uga.corse.themis.monitoring.ehe.*;
import uga.corse.themis.monitoring.GeneralMonitor;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.Simplifier;
import uga.corse.themis.utils.SimplifierFactory;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;
import uga.corse.themis.utils.mapper.SymbolMapper;

/**
 * Migration Monitor
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MyMonitor extends GeneralMonitor implements MonitorRound {


    private static final long serialVersionUID = 1271734629183430244L;
    protected Automata spec;
    protected Integer comps;
    protected transient Representation autRep;
    protected transient Memory<Atom> m;
    protected transient boolean isMonitoring = false;
    protected transient Simplifier simplifier;
    protected transient Verdict currentVerdict;
    protected transient SymbolMapper mapper;
    protected boolean isStarting = false;

    public MyMonitor(Integer id, Automata aut, Integer comps) {
        super(id);
        this.comps = comps;
        this.spec  = aut;
    }

    public void setStart(boolean start) {
        this.isStarting = start;
    }

    //Setup Simplifier
    public void setup() {
        simplifier = SimplifierFactory.spawnDefault();
        m = new MemoryAtoms();
        reset();
    }

    @Override
    public void cleanup() {
        simplifier.stop();
    }

    @Override
    public void monitor(MemoryRO<Atom> observations, MonitorCallback callback) throws Exception {
        if(currentVerdict.isFinal()) {
            callback.requestAbort(id, MonitorCallback.ABORT_TYPE.ABORT_RUN);
            return;
        }

        m.merge(observations);

        //Received formula? Start monitoring if its not
        if (receive()) isMonitoring = true;

        //Are we monitoring?
        if (isMonitoring) {
            //Update one timestamp ahead if we observe something, otherwise trace over
            if (!observations.isEmpty()) {
                while(autRep.getEnd() < t)
                    autRep.tick();
            }

            //Update the representation
            boolean b = autRep.update(m, -1);



            //Find Next
            int next = getNext();

            //Verdicts Updated
            if (b) {
                VerdictTimed v = autRep.scanVerdict();
                if (v.isFinal())
                {
                    this.currentVerdict = v.getVerdict();
                    callback.notifyVerdict(id, v.getVerdict(), new VerdictTimed(v.getVerdict(), v.getTimestamp()));
                    return;
                }
                autRep.dropResolved();
            }

            //Have to Forward
            if (next != getID()) {
                //Pack content from last known state in the automaton
                Representation toSend = autRep.sliceLive();
                send(next, new RepresentationPacket(toSend));
                //Stop monitoring
                isMonitoring = false;
            }

        }
        return;
    }
    @Override
    public Verdict getCurrentVerdict() {
        return currentVerdict;
    }


    @Override
    public void reset() {
        m.clear();
        autRep = new Representation(spec, simplifier);
        currentVerdict = Verdict.NA;
        mapper = new DefaultSymbolMapper();
        isMonitoring = isStarting;
    }

    @Override
    public void communicate() {

    }


    //Get the next monitor to send to
    public int getNext() {
        return  mapper.monitorFor(Expressions.findEarliestObligation(autRep, true));
    }

    /**
     * Check the receipt of representations (EHE) from other monitors
     *
     * @return True if monitor received anything
     */
    protected boolean receive() {
        Message m;
        boolean result = false;
        Representation repr = null;
        while ((m = recv()) != null) {
            result = true;
            RepresentationPacket packet = (RepresentationPacket) m;
            if (repr != null)
                repr.mergeForward(packet.repr);
            else
                repr = packet.repr;
        }
        if (repr != null) {
            if (isMonitoring) {
                autRep.mergeForward(repr);
            }
            else {
                autRep = repr;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "[" + id + " : " + (isStarting ? "active" : "standby") + " ]";
    }

    private int t;
    @Override
    public void notifyRound(int round) {
        t = round;
    }
}
