# THEMIS Tutorial: Creating Your Own Algorithms and Measures

## Overview

* This directory contains necessary project structures for creating a simple project that depends on THEMIS and contains an algorithm and measure (in [src](src/)) along with the Maven [POM file](pom.xml) to compile it.

* The [short](short/) directory contains necessary configuration needed to run a THEMIS experiment, which is used to benchmark your own algorithm with the extra measure created against pre-existing ones.

* The [plot](plot/) directory contains necessary R scripts needed to plot the benchmark.

## Setup

**Important**: Ensure you are running in the docker and are in this sub-folder.
For instructions on the docker, see [docker](../docker).

**Important**: If you choose to rename the example classes and files, you will need to update the Makefile appropriately, we suggest you do not do this for the scope of the tutorial.

Extract the compressed traces using:

```bash
unzip traces.zip
```

Test the compilation of the existing monitoring algorithm with (*note that for the first time Maven will need to download a lot of dependencies*):

```bash
make build
```

If you wish to test the existing algorithm before modifying it skip to Step 1.3.



## Step 1: Understanding and Modifying a Monitoring Algorithm

In this first step, we look at how to use THEMIS to design your own monitoring algorithm.
We provide a copy of the Migration algorithm in [src/example/alg/MyAlgorithm.java](src/example/alg/MyAlgorithm.java).

The goal of this step is to modify it so that, instead of sending the information to the monitor with the earliest obligation, it will perform a round-robin.


### Step 1.1 Modifying the Setup Phase
Open the file [src/example/alg/MyAlgorithm.java](src/example/alg/MyAlgorithm.java) with your favorite text editor (you can do so outside the docker) and take a look at its structure. A monitoring algorithm should implement two methods:

* A method that receives the system specifications and decides how to bootstrap.

```java
public void setSpecification(Map<String, Specification> specs);
```

* A method that provides a set of monitors (this corresponds to the `setup` phase). This method is given a `Topology`.

```java
public Set<? extends Monitor> getMonitors(Topology topology);
```

The `Topolgy` object has the following relevant methods:

```java
// Return the number of components
public int getCountComponents();
/*
 * Returns a graph of component connections.
 * Each node is a component.
 * The edges contained in a set are other components it is connected to.
 */
public Map<String, Set<String>> getGraph();
```

Ultimately, each monitor is responsible of deciding which other monitor to send information to.
Note that the `setup` process can also pass this topology information to the monitor (this depends on how one codes the monitor).

In this example, the setup algorithm creates one `MyMonitor` for each component which is a migration monitor.

**Task:** Modify this setup phase to round-robin. For this, simply ensure that monitor with ID 0 starts first. To do this, modify the following function accordingly:

```java
public static int getStartID(Automata aut);
```

We now move to editing the monitor itself so it becomes round-robin.

### Step 1.2 Modifying the Monitor


Open the file [src/example/alg/MyMonitor.java](src/example/alg/MyMonitor.java) with your favorite text editor (you can do so outside the docker) and take a look at its structure. The key methods to implement are the following:

```java
/**
 * Main monitoring method: monitor receives observations (Read-Only) and a callback.
 * The callback is used to notify the runtime of the verdict.
 */
public void monitor(MemoryRO<Atom> observations, MonitorCallback callback) throws Exception;

/**
 * If the monitor supports Round-based monitoring,
 * it is notified of the round (logical timestamp) using this method.
 */
public void notifyRound(int round);

//Setup the monitor when it is spawned.
public void setup();
//Cleanup the monitor when it is destroyed
public void cleanup();
//Reset method is called when a new trace needs to be read but the configuration has not changed.
public void reset();
```

**Task:** To modify the monitor to include round-robin, you need to change only the following method appropriately to return the next ID in a round-robin fashion:

```java
public int getNext();
```

Note that the monitor ID is stored in `id` or retrieved via `public int getID()` and the total number of components is saved in `comps`.


### Step 1.3 Compiling The Modified Algorithm

Compile by running (the first time will be slow as maven needs to download dependencies):

```bash
make build
```

If all goes well, a jar file containing the modified algorithm is generated under [target](target).
This jar file is then run by THEMIS.

### Step 1.4 Running The Modified Algorithm

THEMIS uses a client-server architecture.
The server node runs the monitoring algorithms.
The client node sends commands to the server (e.g., to deploy the monitor) and can also call the bootstrap.

For this we will need to run inside `screen` or `tmux` both are installed on the docker, use whichever works for you.
To spawn a `screen` use:

```
screen
```

(Cheatsheet) You can use the following shortcuts to create and navigate (for tmux, use `Ctrl+b` instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal

In the first terminal, spawn a node with the following:

```bash
make node
```

**Important Note:** the node needs to be shutdown (`Ctrl+c`) and restarted whenever you recompile your code.

If everything goes well, it should print "Node Reloaded".

Now create a new terminal in the same screen (`Ctrl+a c`)

In the new terminal run:

```bash
make example
```

You can run the default Migration algorithm in THEMIS using:
```bash
make default
```

You can swap back to the node using (`Ctrl+a n`) to see the log of the execution. Some errors might show up due to concurrent aborts when verdicts are generated, you may safely ignore these.

If all is well, a database with stored metrics is generated `run.db`.

Then, run the following command to check that the runs executed:
```bash
make show
```

You may also check other metrics by querying the database:
```bash
sqlite3 run.db -header -column "SELECT run_id, alg, spec, tid, comps, verdict, run_len, msg_num, msg_data FROM bench;"
```

**Note that these metrics are not normalized by length of run.**

For a list of available metrics:

```bash
sqlite3 run.db -header -column "PRAGMA table_info(bench);"
```

You can normalize metrics if need be by properly casting:

```bash
sqlite3 run.db -header -column "SELECT run_id, alg, spec, tid, comps, verdict, run_len, (msg_num*1.0/run_len) as msgnum, (msg_data*1.0/run_len) as msgdata FROM bench;"
```

Or simply explore the database with an external tool like [DB Browser for SQLite (sqlitebrowser)](https://sqlitebrowser.org/).

## Step 2: Understanding Metrics

In this step of the tutorial, we create a new metric to count memory discrete accesses. Note that we refer to Monitor memory, not RAM.

Open the file [src/example/measure/MeasureEx.aj](src/example/measure/MeasureEx.aj) with your favorite text editor (you can do so outside the docker) and take a look at its structure.

The key method to implement is:

```java
//Setup function. The method is called with the node and additional tags
protected void setup(Node n, Map<String, Serializable> tags);
```

Key methods to invoke when setting up a metric are the following:
```java
// Create a category for multiple measures
setDescription(String categoryName);
// Create a measure
// See file for more info
addMeasure(Measure measure);
```

Note that metrics are AspectJ aspects that extend the basic `Instrumentation` aspect.
Moreover, if you are not familiar with AspectJ, THEMIS provides Java friendly hooks.

In the methods below, class `MemoryRO` (for Memory with Read-Only) models memory that cannot be modified; these are the observations passed to the monitor from the component it is attached to

```java
/**
 * This method setups the run.
 * It allows access to the Node which has additional information on the monitoring environment (active monitors, components etc)
 */
protected void setup(Node n, Map<String, Serializable> tags);

/**
 * Executes before starting reading each trace.
 */
protected void runBegin();

/**
 * Executes after finishing reading each trace.
 */
protected void runEnd();

/**
 * Executes at the start of each discrete step.
 */
protected void stepBegin(Monitor mon, MemoryRO<Atom> obs);

/**
 * Executes at the end of each discrete step.
 * This will not execute when the algorithm throws a verdict.
 */
protected void stepEnd(Monitor mon, MemoryRO<Atom> obs);

/**
 * Executes when a verdict is reported by throwing the exception
 * (This indicates that the full algorithm has reported a verdict)
 * This executes instead of stepEnd.
 *
 * @param monid Monitor ID reporting the verdict
 * @param v Verdict reported
 */
protected void stepVerdict(int monid, Verdict v);
```

Additionally, a list of pointcuts for AspectJ can be found in: `uga.corse.themis.measurements.Commons` and can be composed to simplify your instrumentation, these include:

```java
//Start/Stop monitoring pointcuts
public pointcut monitoringStartCall(Map<String, Serializable> tags);
public pointcut monitoringStartExec(Map<String, Serializable> tags);
public pointcut monitoringStopCall();
public pointcut monitoringStopExec();

// Monitoring

//Pointcut to capture Monitoring call. It activates when in the execution of the Monitor function, it is mostly to be combined with other.
public pointcut inMonitor(Monitor mon)
// Execution of the monitor update function for this round
public pointcut monitorStep(Monitor mon, MemoryRO<Atom> obs);
// Discrete time update
public pointcut monitoringStep(Integer t)
//Pointcut to capture sent messages
public pointcut sendMessage(Integer to, Message m)
// Pointcut that is trigerred each time an expression is evaluated
public pointcut evalExpr()

//Pointcut that is triggered when monitor of id monid emits verdict.
// Note, the object is an extra object the monitor can send when it issues the verdict, it's mostly null, unless you have custom algorithms that requires the algorithm outputs some other things with the verdict, it is designed to be a generic parameter passed back to the node so you can implement complex algorithms (notably for reliability/fault tolerance).
public pointcut onVerdict(Integer monid, Verdict verdict, Object obj)
```

For counting memory accesses, we simply track calls to the `Memory` object and increment the number of accesses by 1:

```java
after() : call(* Memory.get(..)) {
  update("ex_memory", 1);
}
```
where `ex_memory` is the name of the column in the database.

Since we already had this metric running for earlier runs (make sure you have `run.db` available and have done Step 1), we can display it:

```bash
sqlite3 run.db -header -column "SELECT run_id, alg, spec, tid, comps, verdict, run_len, ex_memory FROM bench;"
```

**Important Note**: whenever metrics (keys/types) are changed, `run.db` needs to be erased as the database table structure changes. And the node reloaded.

**Task:** Add a metric counting for the number of messages exchanged by the monitors.
Hint: Use pointcut `Commons.sendMessage(Integer to, Message m)`. One can combine it with `inMonitor` to capture the current monitor executing i.e. after(Monitor mon, Integer to, Message m)

### Step 3: Running A Small Experiment

This step requires Step 1 to completed to be able to use the example algorithm.

Let us now compare our own algorithm with the existing ones in THEMIS.
THEMIS allows for multiple parametrized runs to be executed using the Experiment tool. This requires a configuration and setup.

First lets make sure to shutdown the node if its still running, cycle in your screen/tmux to the node (`Ctrl+a n`) then hit `Ctrl+c` to stop it.

A simple configuration is provided in [short](short/), let us go into that folder.

```bash
cd short
```

Launch a (server) node in the `short` folder (Make sure you are in the folder)

```bash
make node
```

On the other terminal (`Ctrl+a n`) also make sure you are in the short folder

```bash
cd short
```

Open [algs.txt](short/algs.txt): you will notice an algorithm class is provided on each line, these are the algorithms we are going to compare against each other.

Open [specs.txt](short/specs.txt): each line will contain a path to an XML specification that will be used for each of the algorithms.

Open [setup.properties](setup.properties): this file is used to guide the experiment, you will find different values that help setup the trace folder, the number of traces to use, the run length (usually a bit longer than traces for timeouts), and the number of components and observations to use (may be less than those included in trace). The test Oracle is used to test each spec and if the test passes the experiment can launch, this is used to verify that LTL formulae included are monitorable.

The names should be self-explanatory in general but we explicit some of them below. Note, that it is not needed to yweak these for the current tutorial.
* tests.skip: indicates the test of the specification should be skipped before monitoring; the test consists in checking monitorability of the specification
* test.discard: if set to true, the tool will stop after testing and generate two files, one with the success specs and one with the failed ones, which you can simply delete and then run it again on the success one.
* traces.maximum: experiment tool will use traces IDs between 0 and this number, this effectively the number of traces you want to run for each (algorithm, spec) pair.
* run.components: when not all components of a traces should be used
* node.host: the IP/port used by the experiment tool to get notified by the remote nodes on completion of runs
* node.targets: these are the nodes that will run your experiment, these should be running, workload is split across multiple ones if there are multiple ones (workload is just the number of traces for now) so if you have 2 nodes and 100 traces, the experiment tool will run 50 traces on each

We are going to use 2 specs and 5 traces so that it completes fast. However it is recommended to try adding all the specs found in the [spec](short/spec) folder to [specs.txt](short/specs.txt) and using at least 50 traces (by setting `traces.maximum=50` in [setup.properties](short/setup.properties)).

Regarding the database. The database has a simple structure consisting of one table called `bench`. Each row in the table corresponds to a completed run (i.e., ended by a verdict). The columns contain information about the run such as the algorithm, the specification, the number of component, the length of the run as well as the various metrics collected during the run.

First ensure that no `run.db` exists under `short`, it is recommended to always delete it before running a new experiment.

Now run the experiment using:

```bash
make experiment
```

*If the specification test fails, simply run it again as sometimes the synthesis fails.*

When the experiment completes check the runs using:

```bash
make show
```

You may also run this nifty normalized query on your data (it is always better to not normalize in the queries but in something like R):

```bash
sqlite3 run.db -header -column "SELECT run_id, alg, spec, tid, comps, verdict, run_len, (msg_num*1.0/run_len) as n_msg_num, (msg_data*1.0/run_len) as n_msgdata, (ex_memory*1.0/run_len) as n_memory_accesses FROM bench;"
```

**Task:** Run a bigger experiment. For this, simply delete `run.db`, then you can use up to 200 traces and the specs in the [specs](short/specs) folder. (This may take a considerable amount of time).

Now cycle to the node (`Ctrl+a n`) and stop it (`Ctrl+c`).

### (Optional) Step 4: Comparing Data

For the purpose of this tutorial, a small `run.db` is fine but it will not show relevant results.
Once you generate a sufficiently large `run.db` in Step 3, you will need to analyze the data.
This step illustrates how you can re-use our R scripts for the comparison.

Go to the folder [bench](plot/bench) (assuming you are in [short](short/)):

```bash
cd ../plot/bench
```

Now generate the csv file for R using (adjust the path accordingly for other databases):
```bash
make gendata DB=../../short/run.db
```

Now plot the data using:

```bash
make plot
```

Use an external file manager to open the PDFs generated, additionally some LaTeX tables are generated.

**Note:** If your data is too little, you may see some warnings, and the graphs may not be significant.

You may also modify [plot.r](plot/bench/plot.r) to adjust the computation to include your metric or draw more graphs.

You can cleanup the generated files by running:

```bash
make clean
```
