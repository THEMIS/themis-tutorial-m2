---
title: THEMIS Tutorial
---

## Reading the README.md Files ##
All README.md files are pre-rendered as HTML for convenience.
In each directory (including this one), the `index.html` file corresponds to a rendered `README.md`.

## Setup

### Using the Docker Container
An environment for the tutorial is provided as a docker container with all needed dependencies.
We provide the Dockerfile to build the image.
For more instructions see the [docker](docker/) folder.

## Tutorial
The tutorial is split into two parts. Each part assumes you are running the docker and are in the appropriate sub-folder. Check the sub-folder `README.md` files for instructions for each part.

* The [first part (compare-algorithms)](compare-algorithms/) consists in creating a monitoring algorithm and a metric and comparing the to pre-existing ones using the metric.

* The [second part (case)](case/) deals with a case study about the monitoring of a smart appartment.
It consists in exploring the case study to see how THEMIS is used in that context, and modifying specifications so as better monitor some specifications.





## Related Papers

* [Antoine El-Hokayem, Yliès Falcone: \
On the Monitoring of Decentralized Specifications: Semantics, Properties, Analysis, and Simulation. \
ACM TOSEM, 29(1): 1:1-1:57 (2020)
](https://doi.org/10.1145/3355181)

* [Antoine El-Hokayem, Yliès Falcone: \
Bringing Runtime Verification Home.
Proceedings of the 18th international conference on Runtime Verification, November 10-13, 2018, Limassol, Cyprus
](https://doi.org/10.1007/978-3-030-03769-7_13)

* [Antoine El-Hokayem, Yliès Falcone: \
Monitoring decentralized specifications. \
Proceedings of the 26th ACM SIGSOFT International Symposium on Software Testing and Analysis, July 10-14, 2017, Santa Barbara, CA, USA
](https://dl.acm.org/citation.cfm?id=3092723)

* [Antoine El-Hokayem, Yliès Falcone: \
THEMIS: a tool for decentralized monitoring algorithms. \
Proceedings of the 26th ACM SIGSOFT International Symposium on Software Testing and Analysis, July 10-14, 2017, Santa Barbara, CA, USA
](https://dl.acm.org/citation.cfm?id=3098224)
