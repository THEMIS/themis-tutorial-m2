# R package installer

# Set default mirror
# Well it is possible to find a better one
local({r <- getOption("repos")
       r["CRAN"] <- "http://cran.r-project.org" 
       options(repos=r)
})

# List needed packages
pkgs <- c("stringi", "reshape2", "ggplot2", "dplyr", "xtable")

# Filter out already installed packaged
#inst <- pkgs[!(pkgs %in% installed.packages()[,"Package"])]

# Install whatever is needed 
#if(length(inst)) install.packages(inst)
install.packages(pkgs)
