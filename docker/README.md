---
title: Docker Image
---

This folder builds a docker image with all needed dependencies to run build and execute all artifacts.
Additionally, the docker provides a server to browse the `README.md` files.

Firstly, make sure you are in the [docker](.) folder:
```bash
cd docker
```


## Creating and Running the Image ##

The following instructions manipulate the docker container, please make sure to have **root** privileges (if necessary) for docker commands.

### Using GNU Make
* Build the docker image  (~ 30 minutes)

```
# make build
```
* Bind all the artifacts to the running docker image and launch a shell in the docker container

```
# make run
```

* Proceed to: [after running](#after-running).

### Without GNU Make ###
* Build the docker image (~ 30 minutes)

```
# docker build --force-rm=true  -t themis-artifact:rv18smarthome .
```

* Run the docker image

```
# docker run --rm -p 8050:8050 -p 8051:8051  -i -v `pwd`/../:/home/user/themis/:rw -t themis-artifact:rv18smarthome
```

## After Running

* It is now possible to use a regular file explorer and programs to browse the files in the directory in parallel, changes done in the docker reflect on the files directly (outside docker).

> **Note:** The docker image mounts all the artifacts (as volumes) in read-write mode, changes done in the docker container will reflect in the original folder.

* You can now also browse the documentation using a browser using the pre-rendered files: [http://localhost:8050/](http://localhost:8050)


## Changing the Artifacts Mountpoints ##

The default artifacts are mounted from the parent directory, i.e.: [`..`](/).

If you wish to use a different workdir than  [`..`](../), it must be an absolute path in the form of `/path/to/workdir`.

To do so, simply provide the path with `WD` as follows:

```
# make run WD=/path/to/workdir
```


## Deleting the Image ##

Once done with using the docker image, delete the created docker processes and images relevant to THEMIS by running:

```
# make clean
```

## Running without Docker

If you are having trouble running the docker or building the docker, here is the additional information needed to run a THEMIS environment.


### Running THEMIS (Mandatory)
`THEMIS` is provided as a JAR with most dependencies embedded. You can find it in [lib](lib/).

* Requires JAVA 8+.
* Requires an LTL/Boolean simplifier ([ltlfilt](https://spot.lrde.epita.fr/ltlfilt.html)) from [spot](https://spot.lrde.epita.fr/)

For more info on spot see:

* [Spot 2.0 — a framework for LTL and ω-automata manipulation, Alexandre Duret-Lutz, Alexandre Lewkowicz, Amaury Fauchille, Thibaud Michaud, Etienne Renault, and Laurent Xu. In Proc. of ATVA'16, LNCS 9938, pp. 122–129. Chiba, Japan, Oct. 2016](https://spot.lrde.epita.fr/citing.html).


### Monitor Synthesis (Optional)
> By default all specifications used in the case study are cached in a cache folder called `cache`, and no calls to the monitor synthesis software will trigger. **So this is only necessary if you aim to write your own properties, or modify existing ones**. To regenerate the properties make sure to clear the cache folder or specify another in the `.env` scripts.

For monitor synthesis one of the two programs is required:

* `ltl2mon` (invoked using command `ltl2mon`) must be on the `$PATH`. We use a patched version that supports STDIN input, you can find it in [third.tgz](third.tgz) (requires extraction). Or you can grab it from the website at [http://ltl3tools.sourceforge.net/](http://ltl3tools.sourceforge.net/) and patch it yourself. The patched script is found in `third/ltl3tools-0.0.7/ltl2mon`.
* `lamaconv` (invoked using command `rtlconv`) must be on the path. We use it as is, you can grab a copy from its website at [http://www.isp.uni-luebeck.de/lamaconv](http://www.isp.uni-luebeck.de/lamaconv)
* By default we use `ltl2mon`, if you wish to use `lamaconv` simple edit the [Makefile](../case/Makefile) for the Smart Home case study and add `-rtlconv` to `FLAGS`, or invoke it with `FLAGS=-rtlconv`.

We thank the authors for making the above tools available, as monitor synthesis is necessary to be able to monitor, for more information on the tools consult:

* `ltl2mon`: [Andreas Bauer, Martin Leucker, Christian Schallhart:
Runtime Verification for LTL and TLTL. ACM Trans. Softw. Eng. Methodol. 20(4): 14:1-14:64 (2011)](http://doi.acm.org/10.1145/2000799.2000800).
* `lamaconv`: [http://www.isp.uni-luebeck.de/lamaconv](http://www.isp.uni-luebeck.de/lamaconv).
