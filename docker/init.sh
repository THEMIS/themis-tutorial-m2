#!/bin/bash
USER=user
HOST_MNT=/home/$USER/themis

# BSD or Linux?
stat --help 1>/dev/null
if [ $? != 0 ]; then
  STATF="stat -f "
else
  STATF="stat -c "
fi

# Set up user/group
GROUPID=`$STATF %g $HOST_MNT`
USERID=`$STATF %u $HOST_MNT`

source /home/$USER/.bashrc


(cd /home/$USER/themis && python -m http.server --bind 0.0.0.0 8050 2>/dev/null 1>/dev/null) &
#(cd /home/$USER/themis && grip 0.0.0.0:8051 2>/dev/null 1>/dev/null) &


echo 
echo "++-- THEMIS Docker Container ----"

echo "root:root" | chpasswd 
echo "+ Passwords:"
echo "+ root's password is 'root'"

if [[ $USERID -eq 0 && $GROUPID -eq 0 ]]
then
  echo "# Warning: Directory '$HOST_MNT' is not mounted"
  echo "# Running as root"
  USER=root
else
  groupadd --gid $GROUPID $USER --non-unique
  useradd --uid $USERID --gid $GROUPID $USER
  echo $USER:$USER | chpasswd 
  echo "+ $USER's password is '$USER'" 
  chown -R $USER:$USER /home/user
fi

echo "+--------------------------------"
echo 
echo "+--------------------------------"
echo "+ Browse the README files:"
echo "+ http://localhost:8050/"
#echo "+ OR http://localhost:8051/"
echo "+--------------------------------"
echo
exec su $USER
