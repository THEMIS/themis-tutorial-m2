---
title: THEMIS Smart Home - Case Study
---

# Orange4Home dataset
The folder [sensors](sensors) contains partial sensors data from the [Orange4Home](https://amiqual4home.inria.fr/fr/) dataset.

We like to thank the [Amiqual4Home](https://amiqual4home.inria.fr/fr/) (ANR-11-EQPX-0002) project team, in particular Stan Borkowski and James Crowley for aiding in the case study and Julien Cumin, for providing the data.

When re-using the data from this artifact, please be sure to consult the authors first, and the following:

 * [Julien Cumin, Grégoire Lefebvre, Fano Ramparany, James L. Crowley. A dataset of routine daily activities in an instrumented home. In International Conference on Buquitous Computing and Ambient Intelligence, 2017](https://rd.springer.com/chapter/10.1007/978-3-319-67585-5_43).



# Tutorial: Exploring and Modifying A Property

In this small tutorial we will be looking at a single ADL property, that of the user napping. We will modify it to illustrate the various effect of sensors on the property.

First make sure you are in the case folder:

```bash
cd ~/themis/case
```

## Step 1: Examine the Property

Open the file [specs/nap.xml](specs/nap.xml) with your favorite editor, and take a look at the structure.

* You will notice first a section defining the components we are using.
These components are associated with the house sensors, and are instantiated accordingly based on CSV files obtained from the dataset, and some initial values.

* The second section contains the set of specifications needed to define our ADL properties. The important specifications to observe here are those linked to the component sensors. For instance we create a simple monitor that simply forwards the component observation to check whether the light is on or not (Notice `component` links to a component defined in the first section).

```xml
<specification id="m_bedroom_luminosity" class="uga.corse.themis.smarthome.specs.SpecSimpleDevice">
    <component>bedroom_luminosity</component>
</specification>
```

* Additional properties are then defined on top of these basic monitors. For the napping property you will notice references to lower monitors are defined using `@` and the setup algorithm will automatically connect the monitors when needed.

```xml
<specification id="napping" class="uga.corse.themis.smarthome.specs.SpecLTLDuration">
    <setLTL><![CDATA[G^3((!@m_bedroom_luminosity) & @m_bedroom_bed_pressure)]]></setLTL>
    <autoconnect>true</autoconnect>
</specification>
```

You can analyze the property by checking dependencies using the following:

```bash
make analyze CFILE=specs/nap.xml
```

The outcome will tell you on which monitor, and sensor atomic propositions each property depends.

You can see more complex properties depend on a much larger depth and breadth of sensors, for a list of all properties take a look at:

```bash
make analyze CFILE=specs/adl/ADL.xml
```

## Step 2: Checking The Property


Running THEMIS requires a server node, then all tools connect to the node and deploy monitors and schedule runs.

**Important:**
If you are already running inside a screen from another part of the tutorial, make sure to kill existing nodes (`Ctrl+c`) by cycling to them (`Ctrl+a n`), and then exiting the command line (`exit`) for all running shells in the screen. You will need to run the node in this path as it is specific to Smart Home with a custom algorithm.

For this we will need to run inside `screen` or `tmux` both are installed on the docker, use whichever works for you.
To spawn a `screen` use:

```
screen
```

(CheetSheet) You can use the following shortcuts to create and navigate (for tmux, use Ctrl+b instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal

In the first terminal spawn a Smart House node with the following:

```bash
make node
```

Now spawn a new terminal (`Ctrl+a c`), and run the following:

```bash
make adl-nap
```

This will take some time as there are 36,000 timestamps in the trace. You can track completion by cycling to the node (`Ctrl+a n`) and see the progress.

Once complete two files are created `orange.db` which contains the metrics data, and `trace.log` which contains verdict information for the specifications.

Using the monitor data, we can compute precision and recall.
This is done by reading the annotations and then checking if our monitors guess in the right interval (precision) and then if they cover the whole interval (recall).
All the other ADL properties will be ignored as we are only focusing on `napping`.

```bash
make verify
```

When done cleanup by running:

```bash
rm orange.db trace.log
```

**Task:** Open [specs/nap.xml](specs/nap.xml) and modify the main napping property with any of the commented formulae, or simply write your own, then run and verify its precision and recall.



# (Optional) Executing The Full Use-Case

## Preliminaries

Whenever monitoring is executed, a trace is generated with all verdicts of all monitors for any given timestamp, and its delay.
It is stored in [trace.log](trace.log).

[A sample trace](trace.zip) is provided with you but is compressed.
To unpack the trace simply execute the following:

```
unzip trace.zip
unzip orange.db.zip
```

Furthermore all metrics of any executed run will be added to the table `bench` in the Sqlite3 database [orange.db](orange.db), the table resulting from our experiment is provided as the default [orange.db.zip](orange.db.zip).



## Analyzing specifications

Analyzing specifications allows you to see atomic propositions and dependencies for all monitors in a given specification file.
This uses the `uga.corse.themis.smarthome.AnalyzeSpec` tool.

To analyze a specification file simply execute:

```
make analyze CFILE=/path/to/spec.xml
```

For example:
```
make analyze CFILE=specs/adl/ADL-M.xml
```

This results in:
```
+ Spec: reading
+ Depth: 2
+ Leafs: [bedroom_closet_door, bedroom_luminosity, bedroom_drawer_1, bedroom_drawer_2, bedroom_bed_pressure]
+ Dependencies (Direct): [dressing, m_bedroom_luminosity, napping]
+ Dependencies (Transitive): [dressing, m_bedroom_luminosity, m_bedroom_drawers, m_bedroom_closet_door, m_bedroom_bed_pressure, napping]
+ reading                        AP^d:   3  | AP^c:   5 | d:   2
```

## Checking Precision and Recall

After generating a trace, it is interesting to consider precision and recall of the ADL properties.
To do so we require the following information:

* Annotations [sensors/annotations.csv](sensors/annotations.csv): contains the manual annotations performed by the tenant in the apartment, and is provided part of the [Orange4Home dataset](sensors/).
* A dictionary [dict.csv](dict.csv): contains the translation of the relevant annotations to the monitor names in the specification.
* A start and end date to consider annotations (for recall), which coincides with the trace.

To verify precision and recall on the experiment trace execute the following:

```
unzip trace.zip
make verify
```

An example output should show:

```
++ sink_usage
Recall: 0.1135 - Tue Jan 31 08:25:03 CET 2017 - Tue Jan 31 08:28:07 CET 2017 (21/185)
Recall: 0.1447 - Tue Jan 31 12:48:21 CET 2017 - Tue Jan 31 12:50:52 CET 2017 (22/152)


--sink_usage                     Precision: 1.0000 Recall: 0.1276 F1: 0.2263
```
Each user annotated interval will appear for a given property.
The parentheses for each interval display the range in seconds and the number of correct guesses within the range.

When checking the precision recall for an arbitrary trace after monitoring, make sure to specify the date bounds correctly, you can adjust those as follows:

```
make verify EXP_START="2017-01-30 07:30:00" EXP_END="2017-01-31 17:30:00"
```

## Replaying Traces

### Note on THEMIS Nodes

`THEMIS` has two parts: a runtime that performs monitoring and a tool that sends all data, information on components and monitors to the runtime.

As such you will require two terminals at least to run `THEMIS`.

> *Note: if you are using the docker image, you can use `screen` or `tmux` to start multiple terminal instances*


To run the experiment you will need at least a terminal per node and one for the execution.
To do so you can use `screen` or `tmux` as follows, to spawn a screen use:

```
screen
```

Then you can use the following to create and navigate (for tmux, use Ctrl+b instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal

### Replaying a Trace
To replay a trace first start a `screen` or `tmux` if you have not.
See the earlier section on how to do so. In the terminal start a node.

```
make node
```

Then create a terminal using:
```
Ctrl+a c
```

Remember, you can always go back to the node terminal using: `Ctrl+a p` or `Ctrl+a n`.

You are now ready to replay traces.
The tool will execute a run and wait for the monitoring to finish.
Upon completion the trace is created and an entry on measurements is added to the database.

Here are various ways to replay traces:

> **Note: A trace replay usually takes around 20-60 mins as it simulates a full work day (36,000 timestamps). Depending on the number of monitors.**

 **Replaying a Single Trace**: check the [Makefile](Makefile) for various targets that play traces, here are example targets (for specifications see the [specs](specs) folder):

 * `adl-only` (ADL): ADL only properties.
 * `adl-house` (ADL+H): ADL properties and the monitors that verify that an activity has happened on a given floor or the house.
 * `adl-inc` (ADL+H+2): Includes `adl-house` but also a monitor to verify that no two activities happened on different floors.
 * `adl-all` (ADL+M): Includes all ADL properties and meta-properties (largest, trace provided with [trace.zip](trace.zip)).
 * `adl-nap`: This attempts to detect only the `napping` property, see [specs/nap.xml](specs/nap.xml) to modify the property.
 * `switch_orange-d` (SW-D): Includes the system property to verify light switches in 2 rooms, specified in a decentralized manner.
 * `switch_orange-c` (SW-C): Includes the system property to verify light switches in 2 rooms, specified in a centralized manner.
 * `day2`, `day3`: Verifies `adl-all` on different days.

 For example `make switch_orange-d`

**Multiple Executions**

* `./run.sh N target`: will execute make target `N` times, for example: `./run.sh 5 adl-only` will replay the `adl-only` target `5` times.
* `./all.sh N`: will execute `N` times, the targets specified sampled in the paper for measurements (`adl-only`, `adl-house`, `adl-inc`, `adl-all`,  `switch_orange-d`, `switch_orange-c`).

> To obtain the provided [orange.db](orange.db) execute `./all.sh 5`. This will take a **significant amount of time**. You can simply override your current results with those pre-computed: `unzip orange.db.zip`.

## Plotting

For plotting the graph showing the ADL schedule and the metrics see the [plot](plot) folder.
