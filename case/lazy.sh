#!/bin/bash
targets=(lazy0 lazy1 lazy2)

for TG in "${targets[@]}"; do
	./run.sh $1 $TG
done
