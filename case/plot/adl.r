library(reshape2)
library(ggplot2)
library(dplyr)
library(xtable)

pdf(NULL)

dat <- read.csv("../trace.log", header=TRUE)



maxround = max(dat$round)

hours <- function(l) {
  return (l/3600)
}

lvadl <- c( "shower_usage",  "sink_usage", "livingroom_watching_tv", "computing", 
           "preparing",  "cooking", "eating", "washing_dishes",
           "dressing",  "reading", "napping", "m_toilet",
           "office_watching_tv")
labadl <- c("shower_usage",  "sink_usage", "livingroom_tv", "computing", 
            "preparing",  "cooking", "eating", "washing_dishes",
            "dressing",  "reading", "napping", "toilet",
            "office_tv")
lvf <- lvadl
res <- dat %>%  filter((mon_ap %in% lvf))

res$mon_ap <- factor(res$mon_ap, 
                  levels=rev(lvadl),
                  labels=rev(labadl))



ggplot(res, aes(x=round, y=factor(mon_ap), fill=verdict)) + geom_tile() + geom_bin2d(binwidth=c(0.3,1)) +
  scale_x_continuous(labels=hours, breaks=seq(0, maxround, by=3600)) +
  scale_fill_manual(values = c("white", "black"), na.value = "darkorange") +
  xlab("Time") + ylab("Properties")+
  theme(legend.position="top") +
  ggsave("trace_adl.pdf")
