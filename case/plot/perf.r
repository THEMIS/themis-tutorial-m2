library(reshape2)
library(ggplot2)
library(dplyr)
library(xtable)
pdf(NULL)
perf <- read.csv("perf.csv", header=TRUE)

gdraw <- function(g) {
  return( g
          + theme_minimal()
          + theme(axis.line = element_line(colour = 'black', size = .5))
          +theme(axis.text = element_text(size = 14))
          +theme(axis.title=element_text(size=14,face="bold"))
  )
}


sam <- perf %>%  
  group_by(tag) %>%
  sample_n(5) %>%
  ungroup()

res <- sam %>%  
  group_by(tag) %>%
  summarize(
    msg=mean(msg_num/run_len),
    comp=mean(simp_total/run_len),
    data=mean(msg_data/run_len),
    sd_msg=sd(msg_num/run_len),
    sd_comp=sd(simp_total/run_len)
  ) %>%
  select(tag, comp, msg, sd_msg, sd_comp)


res$tag <- factor(res$tag, 
                  levels=c("orange-switch-decentralized-2", "orange-switch-centralized-2", "adl-only", "adl-house", "adl-inc", "adl-all"),
                  labels=c("SW-D", "SW-C", "ADL", "ADL+H", "ADL+H+2", "ADL+M")
                  )

res

gdraw(ggplot(data=res, aes(x=tag,y=msg)) + 
    geom_bar(stat="identity",position = "stack", width=0.6) + geom_text(label=sprintf("%.2f",res$msg), vjust=-0.5, size=5)) +
  ylab("#Msgs (Normalized)") + xlab("Properties") + 
  ggsave("prop_msgnum.pdf")

gdraw(ggplot(data=res, aes(x=tag,y=comp)) + geom_bar(stat="identity", position = "stack", width=0.6) + geom_text(label=sprintf("%.2f",res$comp), vjust=-0.5, size=5)) +
  ylab("#Simplifications (Normalized)") + xlab("Properties") + 
  ggsave("prop_comp.pdf")
#gdraw(ggplot(data=res, aes(x=tag,y=data)) + geom_bar(stat="identity") ) + ylab("Data") + xlab("Properties") + ggsave("prop_msgdata.pdf")
